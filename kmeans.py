import pandas as pd
from random import choice
import seaborn as sns
from matplotlib import pyplot as plt
import numpy as np
import sys

# Definition of distance
distance = lambda (x1, y1), (x2, y2): ((x2-x1)**2 + (y2-y1)**2)**0.5
#sse = lambda points, centroid: sum(distance(point, centroid)**2 for point in points)


def main():
    if len(sys.argv) < 3:
        print "This script requires command line arguments which are, in order, K and r.\nUsage: python kmeans.py 10 6"
        exit()
    K = int(sys.argv[-2])
    r = int(sys.argv[-1])

    def kmeans(K):
        # Retrieve data and create tuples of each datum
        df = pd.read_csv('data.csv')
        df['point'] = zip(df.x1, df.x2)

        # Choose K initial cluster centroids randomly without replacement
        centroids = df.point.sample(n=K).reset_index(drop=True)
        # Each cluster will be identified by its centroid's DataFrame index

        # Finds label by finding the minimum distance between centriods and point.
        find_label = lambda point: min(centroids.keys(), key=lambda label: distance(point, centroids[label]))

        # Label datum to match closest cluster centroid for all data
        df['label'] = df.point.apply(find_label)

        # We'll note these labels to see when things stop swapping membership
        memberships = df.label
        first_run = True

        # We'll stop when clusters are no longer in flux
        while memberships is df.label or first_run:
            first_run = False

            # Now we'll calculate the new centroid for each cluster
            for label, group in df.groupby('label'):
                centroids[label] = (group.x1.mean(), group.x2.mean())

            # Label datum to match closest cluster centroid for all data
            df['label'] = df.point.apply(find_label)
        return (centroids, df)

    # Perform r iterations of the kmeans algorithm and grab the clusters with the minimum SSE
    best_df = None
    best_sse = None
    for i in range(r):
        centroids, df = kmeans(K)
        df['squared_error'] = df.apply(lambda x: distance(x.point, centroids[x.label]), axis=1)
        sse = sum(df.squared_error)
        if sse < best_sse or best_sse is None:
            best_sse = sse
            best_df = df
    sns.lmplot(x='x1', y='x2', data=df, hue='label', fit_reg=False)
    print "SSE:", round(best_sse, 2)
    plt.show()


if __name__ == '__main__':
    main()
