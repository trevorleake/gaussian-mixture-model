import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt
import sys

# TRY TO REPLACE THIS
multivariate_gaussian = lambda x, mu, sigma: (2*3.14*np.linalg.det(sigma))**-.5 * 2.718**(-.5*np.dot(np.matmul(np.transpose(x - mu), np.linalg.inv(sigma)), (x-mu)))



def random_positive_semi_definite_tensor(n,m,k):
    temp_tensor = np.random.uniform(size=(n, m, k))
    a = np.asarray([np.tril(matrix) for matrix in temp_tensor])
    b = np.asarray([np.linalg.inv(x) for x in a])
    return np.matmul(a, b)

def soft_assign(data, means, dispersion_matrix, mixing_coefficients):
    K = len(mixing_coefficients)
    memberships = np.zeros((len(data), K))
    for n, datum in enumerate(data):
        for k in range(K):
            numerator = mixing_coefficients[k] * multivariate_gaussian(datum, means[k], dispersion_matrix[k])
            denominator = sum(mixing_coefficients[j] * multivariate_gaussian(datum, means[j], dispersion_matrix[j]) for j in range(K))
            memberships[n][k] = numerator/denominator
    return memberships

def log_likelihood(data, means, dispersion_matrix, mixing_coefficients):
    K = len(mixing_coefficients)
    sum = 0.
    for n, datum in enumerate(data):
        for k in range(K):
            sum += np.log(mixing_coefficients[k] * multivariate_gaussian(datum, means[k], dispersion_matrix[k]))
    return sum

def bottom_line_print(string):
    sys.stdout.write('\r'+string)
    sys.stdout.flush()


def main():
    if len(sys.argv) < 2:
        print "This script requires command line arguments which are, in order, K, r.\nUsage: python gmm.py 3 10"
        exit()

    K = int(sys.argv[-2])
    R = int(sys.argv[-1])
    epsilon = 100.

    # Retrieve data and create tuples of each datum
    df = pd.read_csv('data.csv')
    data = df.values
    N = len(data)

    models = []
    for r in range(R):
        # Initialize all parameters randomly
        mixing_coefficients = np.random.dirichlet(np.ones(K)) # Mixing components should sum to ~1
        means = np.random.uniform(size=(K,2))                 # Any values are allowed as means
        # To ensure the dispersion matrices are always positive semi-definite
        #  we leverage Cholesky decomposition which says that this property is
        #  held by all matrices which are a product of lower triangular matrices
        #  and their inverses.
        dispersion_matrix = random_positive_semi_definite_tensor(K, 2, 2)

        # The main loop will iterate the algorithm until convergence is acheived
        likelihood = None
        previous_likelihood = None
        too_long = False
        iteration = 0
        while not previous_likelihood or abs(likelihood - previous_likelihood) > epsilon or too_long:
            iteration = iteration + 1
            previous_likelihood = likelihood

            ### E-step ###
            # For each datum we find the membership/soft assignment to each cluster
            memberships = soft_assign(data, means, dispersion_matrix, mixing_coefficients)

            ### M-step ###
            # Each parameter is now recalculated based on our new membership values
            effective_membership = np.sum(memberships, axis=0)
            mixing_coefficients = effective_membership/len(data)
            means = np.asarray([scaled_mean/effective_membership[i] for i, scaled_mean in enumerate(np.matmul(np.transpose(memberships), data))])
            dispersion_matrix = np.asarray([sum(memberships[n][k] * np.matmul((datum-means[k]).reshape(-1, 1), (datum-means[k]).reshape(-1, 1).T) for n, datum in enumerate(data))/effective_membership[k] for k in range(K)])

            # Now we'll examine the log-likelihood
            likelihood = log_likelihood(data, means, dispersion_matrix, mixing_coefficients)
            # If we've converged on a solution or our parameters have stopped moving, we're done.
            too_long = iteration/500

            bottom_line_print(
                'Log-likelihood: ' + str(round(likelihood, 2)) + \
                '\tDelta: ' + str(round(abs(likelihood - previous_likelihood), 2) if previous_likelihood else 0) + \
                '\tEpsilon: ' + str(round(epsilon, 2)) + \
                '\tModels built: ' + str(len(models))
            )

        if not too_long:
            models.append({
                'likelihood': likelihood,
                'memberships': memberships,
                'mixing_coefficients': mixing_coefficients,
                'dispersion_matrix': dispersion_matrix,
                'means': means
            })
        else:
            bottom_line_print('Model aborted. Continuing...')

    model = min(models, key=lambda x: x['likelihood'])
    df['label'] = [np.argmax(membership) for membership in model['memberships']]
    sns.lmplot(x='x1', y='x2', data=df, hue='label', fit_reg=False)
    plt.show()
    print '\n---Final Model---'
    print 'Log-likelihood:', round(model['likelihood'])
    print 'Mixing coefficients:', [round(x,2) for x in model['mixing_coefficients']]
    print 'Means:', [(round(x,2), round(y,2)) for (x, y) in model['means']]
    print 'Dispersion matrix:\n', model['dispersion_matrix']





if __name__ == '__main__':
    main()
